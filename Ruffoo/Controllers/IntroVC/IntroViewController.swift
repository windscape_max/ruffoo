//
//  IntroViewController.swift
//  Ruffoo
//
//  Created by Admin on 01.10.2017.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var createNewAccountBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageScrollView: UIScrollView!
    
    private var animations = ["PinJump", "buttonOff", "toster", "buttonOn"]
    private var animationViews: [IntroAnimatedView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
        loadAnimations()
    }
    
    private func setup() {
        self.createNewAccountBtn.layer.cornerRadius = 6
        
        // delete after recording
        self.pageScrollView.layer.borderWidth = 1
        self.pageScrollView.layer.borderColor = UIColor.black.cgColor
        
        self.pageScrollView.delegate = self
        self.pageScrollView.isPagingEnabled = true
        self.pageScrollView.showsHorizontalScrollIndicator = false
        self.pageScrollView.contentSize = CGSize(width: pageScrollView.frame.width * CGFloat(animations.count), height: pageScrollView.frame.height)
        
        self.pageControl.numberOfPages = animations.count
    }
    
    private func loadAnimations() {
        for (index, i) in animations.enumerated() {
            let animationView = IntroAnimatedView(frame: pageScrollView.bounds, animName: i)
            self.pageScrollView.addSubview(animationView)
            animationView.tag = index
            animationView.frame.size = self.pageScrollView.frame.size
            animationView.frame.origin.x = self.pageScrollView.bounds.width * CGFloat(index)
            animationView.animation.center = CGPoint(x: animationView.frame.width / 2, y: animationView.frame.height / 2)
            animationViews.append(animationView)
        }
        if !animationViews.isEmpty {
            animationViews.first?.animation.play()
        }
    }

    // MARK: - UIScrollViewDelegate
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        animationViews[Int(page)].animation.play()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        pageControl.currentPage = Int(page)
    }
    
    @IBAction func createNewAccountAction(_ sender: UIButton) {
        
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        
    }
}
