//
//  IntroAnimatedView.swift
//  Ruffoo
//
//  Created by Admin on 03.10.2017.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import Lottie

protocol LOTAnimationDelegate: class {
    func playAnimation()
    func pauseAnimation()
    func stopAnimation()
}

class IntroAnimatedView: UIView {

    var animation: LOTAnimationView!
    
    convenience init(frame: CGRect, animName: String) {
        self.init(frame: frame)
        animation = LOTAnimationView(name: animName)
        self.addSubview(animation)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
